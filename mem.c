#include "mem.h"
#include "common.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

// Pointeur vers le premier bloc libre
p_fb first_fb;

// Pointeur vers le premier bloc occupé
p_fb first_ob;

void mem_init(void* mem, size_t taille)
{
	assert(mem == get_memory_adr());
	assert(taille == get_memory_size());

	first_fb = (p_fb) mem;
	first_fb->next = NULL;
	first_fb->size = taille;
	first_ob = NULL;

	mem_fit(&mem_fit_first);
}

void mem_show(void (*print)(void *, size_t, int)) {
	p_fb current = first_fb;
    while (current != NULL) {
        print(current, current->size,1);
        current = current->next;
    }
    p_fb current_ob = first_ob;
    while (current_ob != NULL) {
    	print((char*)(current_ob) + sizeof(fb), current_ob->size,0);
    	current_ob = current_ob->next;
    }
}

static mem_fit_function_t *mem_fit_fn;
void mem_fit(mem_fit_function_t *f) {
	mem_fit_fn=f;
}

size_t memory_alignment(size_t size)
{
    // on arondie la taille sur un multiple de ALIGNMENT
    if (size % ALIGNMENT != 0)
        return size + (ALIGNMENT - (size % ALIGNMENT)) + sizeof(fb);
    else
        return size + sizeof(fb);
}

void *mem_alloc(size_t taille) {
	__attribute__((unused)) /* juste pour que gcc compile ce squelette avec -Werror */
	size_t ob_size = memory_alignment(taille);
	p_fb fb = mem_fit_fn(first_fb,ob_size);
	// Cas ou il n'y a aucune zone libre assez grande
	if (fb == NULL){
		return NULL;
	}

	// Pour faciliter la suite on cherche le bloc libre précédent le bloc libre utilisé
	p_fb iterator = first_fb;
	p_fb previous = NULL;
	while (iterator!=NULL && iterator!=fb){
		previous = iterator;
		iterator = iterator->next;
	}

	// Cas ou il y a assez de places pour une zone libre après la zone occupée
	if (fb->size - ob_size >= sizeof(struct fb)){
		// On créée la nouvelle zone libre
		p_fb new_fb = (p_fb)((char*)fb + ob_size);
		new_fb->size = fb->size - ob_size;
		new_fb->next = fb->next;
		// Cas1 : on est au début de la mémoire
		if (previous==NULL){
			first_fb = new_fb;
		}
		// Cas2 : le reste du temps
		else{
			previous->next = new_fb;
		}
	}
	// Cas ou il n'y a de la place que pour une zone occupée
	else{
		// Cas1 : on est au début de la mémoire
		if (previous==NULL){
			first_fb=first_fb->next;
		}
		// Cas2 : le reste du temps
		else{
			previous->next = fb->next;
		}
	}

	// On crée le nouveau bloc occupé
	// Cas1 : c'est le premier bloc occupé
	if(first_ob==NULL){
		first_ob = fb;
		if(taille==0){
			first_ob->size = taille;
		}else{
			first_ob->size = taille + (ALIGNMENT - (taille % ALIGNMENT));
		}
		first_ob->next = NULL;
	}
	// Cas2 : le reste du temps
	else{
		iterator = first_ob;
		while (iterator!=NULL){
			previous = iterator;
			iterator = iterator->next;
		}
		previous->next = fb;
		if(taille==0){
			fb->size = taille;
		}else{
			fb->size = taille + (ALIGNMENT - (taille % ALIGNMENT));
		}
		fb->next = NULL;
	}
	// On renvoie l'adresse de l'ancienne zone libre + la structure contenant les informations sur cette zone
	return (char*)(fb) + sizeof(struct fb);
}


void mem_free(void* mem) {
	p_fb ob = (p_fb)((char*)mem - sizeof(struct fb));

	p_fb iterator = first_ob;
	p_fb previous = NULL;
	while (iterator!=NULL && iterator!=ob){
		previous = iterator;
		iterator = iterator->next;
	}

	if (iterator==NULL)
	{
		printf("Le bloc à libérer n'existe pas\n");
		return;
	}

	// On retire le bloc de la liste des blocs occupés
	// Cas1 : on est au début de la liste
	if (previous==NULL){
		first_ob = first_ob->next;
	}
	// Cas2 : le reste du temps
	else{
		previous->next = ob->next;
	}

	// On ajoute le bloc dans la liste des blocs libres
	iterator = first_fb;
	previous = NULL;
	while (iterator!=NULL && iterator < ob){
		previous = iterator;
		iterator = iterator->next;
	}
	// Cas1 : on est au début de la liste
	if (previous==NULL){
		first_fb = ob;
		first_fb->next = iterator;
	}
	// Cas2 : le reste du temps
	else{	
		previous->next = ob;
		ob->next = iterator;
	}
	// On modifie la taille du bloc, pour représenter la taille totale du bloc ( structure comprise )
	ob->size += sizeof(struct fb);

	// On fusionne la zone si possible
	// Avec la zone suivante
	p_fb next_bloc = (p_fb)((char*)(ob) + ob->size);
	if(next_bloc==ob->next){
		ob->next = next_bloc->next;
		ob->size += next_bloc->size;
	}
	// Avec la zone précédente //TODO
	iterator = first_fb;
	p_fb previous_bloc = NULL;
	while (iterator!=NULL && iterator!=ob){
		previous_bloc = iterator;
		iterator = iterator->next;
	}
	if(previous_bloc!=NULL && (p_fb)((char*)(previous_bloc) + previous_bloc->size)==ob){
		previous_bloc->next = ob->next;
		previous_bloc->size += ob->size;
	}
	return;
}

// Param : struct fb *list : variable globale first
struct fb* mem_fit_first(struct fb *list, size_t size) {
	p_fb iterateur = list;
	while( iterateur != NULL && iterateur->size < size ){
		iterateur = iterateur->next;
	}
	return iterateur;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	p_fb ob = (p_fb)((char*)zone - sizeof(struct fb));
	if (ob!=NULL){
		return ob->size;
	}
	return 0;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {
	return NULL;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	return NULL;
}
